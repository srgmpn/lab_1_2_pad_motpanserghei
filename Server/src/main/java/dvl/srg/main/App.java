package dvl.srg.main;

import dvl.srg.factory.FactoryIService;
import dvl.srg.interf.IService;
import dvl.srg.model.Message;
import dvl.srg.utils.TimerHelper;
import dvl.srg.winlog.WindowLog;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * start MainBorcker
 * args : [listener port def = 9999] [port second listener   def = 10000] [port second listener queue  def = 10001] [port for receive pings def = 11111]
 * start Second Broccker
 * args : [listener port def = 9999] [port second listener   def = 10000] [port second listener queue  def = 10001] [port for receive pings def = 11111] [param = "s" | for second brocker]
 */

public class App  {

    private static Map<String, Integer> usersDates = new HashMap<>();
    private static AtomicBoolean isOk = new AtomicBoolean(true);
    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private static int mainPort;
    private static int secondListenerPort;
    private static int secBrockerReceiverServicePort;
    private static int pingPort;

    public static void main(String[] args) {
        //fill user map with details
        fillUserDatesMap();

        ExecutorService executorService = null;
        //message queue
        ArrayBlockingQueue<Message> messages = new ArrayBlockingQueue<>(30);

        if (args.length < 4 || args.length > 5) {
            System.out.println("Incorrect nr args");
            return;
        }

        if (!setupPortsValues(args)) {
            System.out.println("Ports values eroars");
            return;
        }

        if (args.length == 4) {
            executorService = Executors.newFixedThreadPool(4);
            winLogger.setWindTitle("Main Brocker");
            System.out.println("---------- MAIN BROCKER START ---------------");
            winLogger.showLog("---------- MAIN BROCKER START ---------------");
            //start main brocker services
            startMainBrockerServices(executorService, messages, false, mainPort, secondListenerPort, secBrockerReceiverServicePort);

            //start recevice ping from main process
            IService recPingService = FactoryIService.newServiceInstance(pingPort, isOk, "receivePing");
            executorService.submit(recPingService);
        }

        if (args.length == 5) {
            executorService = Executors.newFixedThreadPool(6);
            System.out.println("---------- SECOND BROCKER START ---------------");

            winLogger.setWindTitle("Second Brocker");
            winLogger.showLog("---------- SECOND BROCKER START ---------------");

            //start main server listener
            IService secondServiceListener = FactoryIService.newServiceInstance(secondListenerPort, messages, true, true, secBrockerReceiverServicePort, isOk);
            executorService.submit(secondServiceListener);

            IService secondServiceListenerProcessMessage = FactoryIService.newServiceInstance(secBrockerReceiverServicePort, messages, isOk);
            executorService.submit(secondServiceListenerProcessMessage);

            IService sendPingService = FactoryIService.newServiceInstance("127.0.0.1", pingPort, isOk);
            executorService.submit(sendPingService);

            while (isOk.get()) {
                TimerHelper.sleep(450);
            }

            winLogger.showLog("\n---------- SECOND BROCKER -> START MAIN BROCKER ---------------");
            System.out.println("---------- SECOND BROCKER -> START MAIN BROCKER ---------------");

            startMainBrockerServices(executorService, messages, true, mainPort, secondListenerPort, secBrockerReceiverServicePort);

        }

        try {
            executorService.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //start main Brocker services
    private static void startMainBrockerServices(ExecutorService executorService,
                                                 ArrayBlockingQueue<Message> messages, boolean isSecondServer, int mainPort,
                                                 int secondListenerPort, int secBrockerReceiverServicePort) {
            IService serviceListener = FactoryIService.newServiceInstance(mainPort, messages,
                    true, false, secondListenerPort, new AtomicBoolean(true));
            executorService.submit(serviceListener);

            //start service routing
            IService serviceRouting = FactoryIService.newServiceInstance(messages, usersDates, isSecondServer,
                    new AtomicBoolean(true), secBrockerReceiverServicePort);
            executorService.submit(serviceRouting);
    }

    private static boolean setupPortsValues(String args[]) {

        try {

            mainPort = Integer.parseInt(args[0]);
            secondListenerPort = Integer.parseInt(args[1]);
            secBrockerReceiverServicePort = Integer.parseInt(args[2]);
            pingPort = Integer.parseInt(args[3]);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    //fill users data
    private static void fillUserDatesMap() {
        usersDates.put("srg", 20000);
        usersDates.put("john", 20001);
        usersDates.put("grid", 20002);
        usersDates.put("andy", 20003);
    }
}
