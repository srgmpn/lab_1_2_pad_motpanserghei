package dvl.srg.controller;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ServerLogController {

    private Stage stage;
    private Scene scene;

    public ServerLogController() {

    }

    @FXML
    private void initialize() {

    }

    @FXML
    public void start() {
        try{
            scene = new Scene((Parent) FXMLLoader.load(getClass().getResource("/serverlog.fxml")));

        } catch (Exception e) {
            e.printStackTrace();
        }

        stage = new Stage();
        stage.setTitle("Client Window Logs");
        stage.setScene(scene);
        stage.show();
    }
}
