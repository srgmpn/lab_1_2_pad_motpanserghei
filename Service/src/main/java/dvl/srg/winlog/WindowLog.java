package dvl.srg.winlog;

import javax.swing.*;
import java.awt.*;


public class WindowLog extends JFrame {

    private final JTextArea chatWindow;
    private static String windTitle;
    private static WindowLog INSTANCE;

    private WindowLog() {
        //super(windTitle);
        //add(userText, BorderLayout.NORTH);
        chatWindow = new JTextArea();
        add(new JScrollPane(chatWindow), BorderLayout.CENTER);
        setSize(300, 700);
        setVisible(true);
    }

    //singletone
    //double looking check
    public static WindowLog getWinLogger() {
        if (INSTANCE == null) {
            synchronized (WindowLog.class) {
                if (INSTANCE == null){
                    INSTANCE = new WindowLog();
                }
            }
        }
        return INSTANCE;
    }

    //append log to window log
    public void showLog(String log) {
       // System.out.println(log);
        //SwingUtilities.invokeLater(() -> chatWindow.append(log));
        chatWindow.append(log);
    }

    public void setWindTitle(String title) {
        setTitle(title);
    }
}