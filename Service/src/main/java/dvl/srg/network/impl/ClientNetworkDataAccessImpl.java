package dvl.srg.network.impl;

import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;
import dvl.srg.network.abstr.AbstractNetworkDataAccess;
import dvl.srg.winlog.WindowLog;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;


public class ClientNetworkDataAccessImpl extends AbstractNetworkDataAccess {

    private static final WindowLog winLogger = WindowLog.getWinLogger();
    private int port;

    public ClientNetworkDataAccessImpl(int port, Socket connection, ExecutorService service) {
        super(connection, service);
        this.port = port;
    }

    @Override
    protected void handleMessage(Message receivedMessage) throws InterruptedException {
        System.out.println("Client received message : " + receivedMessage.toString());
        winLogger.showLog("\nClient received message : " + receivedMessage.toString());
        writeDataToFile(receivedMessage);

    }

    private void writeDataToFile(Message receivedMessage) {
        Path path = null;
        if ( port % 2 == 0) {
            path = getPath(receivedMessage.getUsernameTo(), "in", receivedMessage.getUsernameTo() + System.currentTimeMillis(), "json");
        } else {
            path = getPath(receivedMessage.getUsernameTo(), "in", receivedMessage.getUsernameTo() + System.currentTimeMillis(), "xml");
        }

        IDataAccess writer = FactoryIDataAccess.newIDataAccessInstance(port, path, receivedMessage);
        writer.writeAsync();

    }

    private Path getPath(String userName, String type, String name, String fileType) {
        Path newFilePath = null;
        try {
            Files.createDirectories(Paths.get(Paths.get("").toAbsolutePath().toString() + "/files/" + userName + "/" + type));
            newFilePath = Paths.get(Paths.get("").toAbsolutePath().toString() + "/files/" + userName +"/" + type +"/", name + "." + fileType);
            Files.createFile(newFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFilePath;
    }
}
