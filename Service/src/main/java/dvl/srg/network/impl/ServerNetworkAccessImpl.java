package dvl.srg.network.impl;

import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.filesystem.XmlFSDataAccesImpl;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;
import dvl.srg.network.abstr.AbstractNetworkDataAccess;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.winlog.WindowLog;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ServerNetworkAccessImpl extends AbstractNetworkDataAccess {

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private boolean isServer;
    private boolean isSecondProcess;
    private int secondProcessPort;
    private ArrayBlockingQueue<Message> messages;

    public ServerNetworkAccessImpl(Socket connection,
                                   int secondProcessPort,
                                   ArrayBlockingQueue<Message> messages,
                                   boolean isServer,
                                   boolean isSecondProcess,
                                   ExecutorService service) {
        super(connection, service);
        this.isServer = isServer;
        this.isSecondProcess = isSecondProcess;
        this.messages = messages;
        this.secondProcessPort = secondProcessPort;
    }

    public ServerNetworkAccessImpl(String host,
                                   int port,
                                   String message,
                                   ExecutorService service) {
        super(host, port, message, service);
    }

    @Override
    protected void handleMessage(Message receivedMessage) throws InterruptedException {

        if (isServer && isSecondProcess) {
            winLogger.showLog("\nSecond process handle message: " + receivedMessage.toString());
            System.out.println("Second process handle message");
            //System.out.println(receivedMessage.toString());
            writeDataToQueue(receivedMessage);
            return;
        }

        if (isServer && !isSecondProcess) {
            System.out.println("Main process handle message");
            System.out.println("Write to queue");
            winLogger.showLog("\nMain process handle message");
            writeDataToQueue(receivedMessage);
            writeToLocalFolder(receivedMessage);

            System.out.println("Send to second Process...");
            winLogger.showLog("\nSend to second process...");
            sendMessageToSecondProcess(receivedMessage);
        }
    }

    private void sendMessageToSecondProcess(Message receivedMessage) {
        IDataAccess sendData = FactoryIDataAccess.newIDataAccessInstance("127.0.0.1",
                secondProcessPort,
                JsonFormaterHelper.toJson(receivedMessage),
                Executors.newFixedThreadPool(1));
        sendData.writeAsync();
    }

    private void writeToLocalFolder(Message receivedMessage) {
        Path path = getPath(receivedMessage.getUsernameFrom(), "in", receivedMessage.getUsernameFrom()  + System.currentTimeMillis());
        //write default in xml format
        IDataAccess writeToFolder = FactoryIDataAccess.newIDataAccessInstance(1, path, receivedMessage);
        writeToFolder.writeAsync();

        path = getPath(receivedMessage.getUsernameTo(), "out", receivedMessage.getUsernameTo()  + System.currentTimeMillis());
        ((XmlFSDataAccesImpl)writeToFolder).setFilePath(path);
        writeToFolder.writeAsync();
    }

    //write data to queue
    private void writeDataToQueue(Message receivedMessage) throws InterruptedException {
        System.out.println("Put message to queue...");
        winLogger.showLog("\nPut message to queue...");
        messages.put(receivedMessage);
    }

    private Path getPath(String userName, String type, String name) {
        Path newFilePath = Paths.get(Paths.get("").toAbsolutePath().toString() + "/serverLogs/files/" + userName +"/" + type  + "/");
        if (!Files.exists(newFilePath)) {
            try {
                Files.createDirectories(Paths.get(Paths.get("").toAbsolutePath().toString() + "/serverLogs/files/" + userName +"/" + type));

            } catch (IOException e) {
                System.err.println(e);
            }
        }
        newFilePath = Paths.get(Paths.get("").toAbsolutePath().toString() + "/serverLogs/files/" + userName +"/" + type +"/", name + ".xml");
        try {
            Files.createFile(newFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newFilePath;
    }
}
