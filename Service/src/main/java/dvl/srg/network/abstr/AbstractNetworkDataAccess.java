package dvl.srg.network.abstr;

import dvl.srg.factory.FactoryIOConnections;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;
import dvl.srg.winlog.WindowLog;

import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;


public abstract class AbstractNetworkDataAccess implements IDataAccess{

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private Socket connection;
    private ExecutorService service;

    private String message;
    private String host;
    private int port;

    private Message receivedMessage;

    public AbstractNetworkDataAccess(Socket connection,
                                     ExecutorService service) {
        this.connection = connection;
        this.service = service;
    }

    public AbstractNetworkDataAccess(String host,
                                     int port,
                                     String message,
                                     ExecutorService service) {
        this.host = host;
        this.port = port;
        this.message = message;
        this.service = service;
    }

    @Override
    public void readAsync() {
        try {
            Future<Message> messageFuture = service
                    .submit(FactoryIOConnections.newHandleInputConnectionInstance(connection));
            receivedMessage = messageFuture.get();
            winLogger.showLog("\nMessage From Service Listener : " + receivedMessage.toString());
            System.out.println("Listener: " + receivedMessage.toString());
            handleMessage(receivedMessage);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeAsync() {
        service.submit(FactoryIOConnections.newHandleOutputConnectionInstance(host, port, message));
    }


    protected abstract void handleMessage(Message receivedMessage) throws InterruptedException;
}
