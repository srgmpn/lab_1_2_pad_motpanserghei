package dvl.srg.network;


import dvl.srg.model.Message;
import dvl.srg.winlog.WindowLog;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.utils.XmlFormaterHelper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StringReader;
import java.net.Socket;
import java.util.concurrent.Callable;

public class HandleInputConnnection implements Callable<Message>{

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private Socket connection;
    private ObjectInputStream input;
    private String stringMessage;
    protected Message message;

    public HandleInputConnnection(Socket connection) {
        this.connection = connection;
    }

    @Override
    public Message call() throws Exception {
        try {
            setupStreams();
            getInputStreamMessage();
            buildMessageObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return message;
    }

    // get stream to receive and send data
    private void setupStreams() throws IOException {
        input = new ObjectInputStream(connection.getInputStream());
    }

    //get inputSream message
    private void getInputStreamMessage() throws IOException, ClassNotFoundException {
        stringMessage = (String) input.readObject();
        winLogger.showLog("\nReceived object message: " + "\n" + stringMessage);
        System.out.println("Received object message: " + "\n" + stringMessage);
    }

    private void buildMessageObject() {
        if (stringMessage.startsWith("<")) {
            message = XmlFormaterHelper.fromXml(new StringReader(stringMessage));
            return;
        }
        message = JsonFormaterHelper.fromJson(stringMessage);
    }

    //close streams and socket after
    private void closeConnection() {
        try {
            input.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
            // logger.error(e.getMessage());
        }
    }
}
