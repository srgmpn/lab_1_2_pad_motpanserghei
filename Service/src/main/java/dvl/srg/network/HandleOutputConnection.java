package dvl.srg.network;


import dvl.srg.model.Message;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.winlog.WindowLog;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class HandleOutputConnection implements Runnable {

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private String host;
    private int port;

    private Socket connection;
    private ObjectOutputStream output;
    private String messageString;

    public HandleOutputConnection(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public HandleOutputConnection(String host, int port, String message) {
        this(host, port);
        this.messageString = message;
    }

    @Override
    public void run() {
        try {
            connectToServer();
            setupStreams();
            sendData();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeCrap();
        }
    }

    // connect to server
    private void connectToServer() throws IOException {
        connection = new Socket(InetAddress.getByName(host), port);
    }

    // get stream to receive and send data
    private void setupStreams() throws IOException {
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
    }

    // send a message to server
    private void sendData() throws IOException {
        winLogger.showLog("\nSend to :" + port + " message : " + messageString);
        System.out.println("Send to :" + port + " message : " + messageString);
        output.writeObject(messageString);
        output.flush();
    }

    //convert
    private void convertMessageToString(Message message) {
        messageString = JsonFormaterHelper.toJson(message);
    }

    // close streams and sockets after
    private void closeCrap() {
        try {
            output.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
