package dvl.srg.model;

import java.io.Serializable;

public class Message implements Serializable {

    private static final long serialVersionUID = -4930213722583855375L;

    private String usernameFrom;

    private String usernameTo;

    private String messageText;

    public Message() {

    }

    public Message(String usernameFrom, String usernameTo, String messageText) {
        this.usernameFrom = usernameFrom;
        this.usernameTo = usernameTo;
        this.messageText = messageText;
    }

    public String getUsernameFrom() {
        return usernameFrom;
    }

    public void setUsernameFrom(String usernameFrom) {
        this.usernameFrom = usernameFrom;
    }

    public String getUsernameTo() {
        return usernameTo;
    }

    public void setUsernameTo(String usernameTo) {
        this.usernameTo = usernameTo;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    @Override
    public String toString() {
        return "Message{" +
                "usernameFrom='" + usernameFrom + '\'' +
                ", usernameTo='" + usernameTo + '\'' +
                ", messageText='" + messageText + '\'' +
                '}';
    }
}
