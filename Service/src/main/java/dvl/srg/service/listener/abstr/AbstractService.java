package dvl.srg.service.listener.abstr;


import dvl.srg.interf.IService;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractService implements IService {

    protected volatile AtomicBoolean isOk;

    public AbstractService(AtomicBoolean isOk) {
        this.isOk = isOk;
    }

    @Override
    public void run() {
        service();
    }

}
