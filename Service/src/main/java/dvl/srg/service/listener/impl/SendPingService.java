package dvl.srg.service.listener.impl;

import dvl.srg.service.listener.abstr.AbstractService;
import dvl.srg.winlog.WindowLog;
import dvl.srg.utils.TimerHelper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;


public class SendPingService extends AbstractService {

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private String host;
    private int port;
    private Socket connection;
    private ObjectInputStream input;
    private String recMessage;

    public SendPingService(String host, int port, AtomicBoolean isOk) {
        super(isOk);
        this.host = host;
        this.port = port;
        this.isOk = isOk;
    }

    @Override
    public void service() {
        while (isOk.get()) {
            try {
                connectToServer();
                System.out.println("-> Send Ping");
                winLogger.showLog("\n-> Send Ping");
                setInputStream();
                receiveResponse();
                handleMessage();
                TimerHelper.sleep(5000);

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                isOk.set(false);
            } finally {
                closeCrap();
            }
        }
    }

    // connect to server
    private void connectToServer() throws IOException {
        //showMessage("\n Attempting connection... \n");
        connection = new Socket(InetAddress.getByName(host), port);
        //showMessage("Connected to " + connection.getInetAddress().getHostName());
        connection.setSoTimeout(5000);
    }

    private void setInputStream() throws IOException {
        input = new ObjectInputStream(connection.getInputStream());
    }

    //receive response
    private void receiveResponse() throws IOException, ClassNotFoundException {
        recMessage = (String) input.readObject();
    }

    private void handleMessage() {
        if (!recMessage.equalsIgnoreCase("OK")) {
            isOk.set(false);
            return;
        }
    }

    // close streams and sockets after
    private void closeCrap() {
        try {
            input.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
