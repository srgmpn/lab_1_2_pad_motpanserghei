package dvl.srg.service.listener.abstr;


import dvl.srg.utils.Constants;
import dvl.srg.winlog.WindowLog;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class ServiceListener extends AbstractService {

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    protected int port;
    private ServerSocket serviceSocket;
    private ExecutorService service;


    public ServiceListener(int port, AtomicBoolean isOk) {
        super(isOk);
        this.port = port;
        service = Executors.newFixedThreadPool(Constants.NR_THREAD_CONNECTIONS);
    }

    @Override
    public void service() {
        try {
            System.out.println("ServiceListener wait connections...");
            winLogger.showLog("\nServiceListener wait connections...");
            serviceSocket = new ServerSocket(port, Constants.BACKLOG); //default backlog 50
            //serviceSocket.setSoTimeout(5000);
            while (isOk.get()) {
                Socket socket = waitForConnection();
                try {
                    handleConnection(socket, service);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //wait for a connection
    private Socket waitForConnection() throws IOException{
        return serviceSocket.accept();
    }

    protected abstract void handleConnection(Socket socket, ExecutorService service) throws InterruptedException;
}
