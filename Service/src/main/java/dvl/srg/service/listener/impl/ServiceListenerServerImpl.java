package dvl.srg.service.listener.impl;


import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;
import dvl.srg.service.listener.abstr.ServiceListener;

import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServiceListenerServerImpl extends ServiceListener {

    private ArrayBlockingQueue<Message> messages;
    private boolean isServer;
    private boolean isSecondProcess;
    private int secondProcessPort;


    public ServiceListenerServerImpl(int port,
                                     ArrayBlockingQueue<Message> messages,
                                     boolean isServer,
                                     boolean isSecondProcess,
                                     int secondProcessPort,
                                     AtomicBoolean isOk) {
        super(port, isOk);
        this.messages = messages;
        this.isServer = isServer;
        this.isSecondProcess = isSecondProcess;
        this.secondProcessPort = secondProcessPort;
    }

    @Override
    protected void handleConnection(Socket socket, ExecutorService service) {
        IDataAccess readData = FactoryIDataAccess.newIDataAccessInstance(socket, secondProcessPort,
                messages, isServer, isSecondProcess, service);
        readData.readAsync();
    }
}
