package dvl.srg.service.listener.impl;

import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.interf.IDataAccess;
import dvl.srg.service.listener.abstr.ServiceListener;

import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;


public class ServiceListenerClientImpl extends ServiceListener {


    public ServiceListenerClientImpl(int port, AtomicBoolean isOk) {
        super(port, isOk);
    }

    @Override
    protected void handleConnection(Socket socket, ExecutorService service) throws InterruptedException {
        IDataAccess readData = FactoryIDataAccess.newIDataAccessInstance(port, socket, service);
        readData.readAsync();
    }
}
