package dvl.srg.service.listener.impl;

import dvl.srg.model.Message;
import dvl.srg.service.listener.abstr.AbstractService;
import dvl.srg.utils.Constants;
import dvl.srg.winlog.WindowLog;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServiceListener2PClearQueueImpl extends AbstractService {

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private ArrayBlockingQueue<Message> messages;
    private int port;
    private ServerSocket serviceSocket;

    public ServiceListener2PClearQueueImpl(int port,
                                           ArrayBlockingQueue<Message> messages,
                                           AtomicBoolean isOk) {
        super(isOk);
        this.port = port;
        this.messages = messages;
    }

    @Override
    public void service() {

        try {
            serviceSocket = new ServerSocket(port, Constants.BACKLOG); //default backlog 50
            serviceSocket.setSoTimeout(5000);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (isOk.get()) {
            try {
                System.out.println("SecondBorcker ServiceListener wait notitfication...");
                winLogger.showLog("\nSecondBorcker ServiceListener wait notitfication...");

                Socket socket = waitForConnection();
                try {
                    handleConnection();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } catch (IOException e) {
                //e.printStackTrace();
                /*don't show error*/
            }
        }
    }

    //wait for a connection
    private Socket waitForConnection() throws IOException{
        return serviceSocket.accept();
    }

    private void handleConnection() throws InterruptedException {
        //remove message from queue
        System.out.println("Remove message form queue...");
        winLogger.showLog("\nSecondBorcker: Remove message form queue...");
        messages.take();
        winLogger.showLog("\nSecondBorcker: Message removed");
        System.out.println("Message removed");
    }
}
