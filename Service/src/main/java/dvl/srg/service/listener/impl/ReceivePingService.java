package dvl.srg.service.listener.impl;

import dvl.srg.service.listener.abstr.AbstractService;
import dvl.srg.winlog.WindowLog;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;


public class ReceivePingService extends AbstractService {

    static final WindowLog winLogger = WindowLog.getWinLogger();

    private int port;
    private ServerSocket serviceSocket;
    private Socket connection;
    private ObjectOutputStream output;

    public ReceivePingService(int port, AtomicBoolean isOk) {
        super(isOk);
        this.port = port;
        this.isOk = isOk;
    }

    @Override
    public void service() {

        try {
            serviceSocket = new ServerSocket(port); //default backlog 50
            while (isOk.get()) {
                System.out.println("ServiceListener wait ping...");
                winLogger.showLog("\nServiceListener wait ping...");
                connection = waitForConnection();
                setupOutputStream();
                winLogger.showLog("\n-> Ping received");
                System.out.println(" -> Ping received");
                sendResponsePing();
                closeConnections();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //wait for a connection
    private Socket waitForConnection() throws IOException {
        return serviceSocket.accept();
    }

    private void setupOutputStream() throws IOException {
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
    }

    //send response
    private void sendResponsePing() throws IOException {
        output.writeObject("ok");
    }

    private void closeConnections() throws IOException {
        output.close();
        connection.close();
    }
}
