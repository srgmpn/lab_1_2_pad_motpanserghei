package dvl.srg.factory;

import dvl.srg.model.Message;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.utils.XmlFormaterHelper;

/**
 * Created by administrator on 9/28/15.
 */
public class FactoryDataType {

    public static String getUserFormatStringMessage (int port, Message message) {
        if (port % 2 == 0) {
            return JsonFormaterHelper.toJson(message);
        }
        return XmlFormaterHelper.toXml(message);
    }
}
