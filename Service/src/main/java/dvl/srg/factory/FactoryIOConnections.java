package dvl.srg.factory;

import dvl.srg.network.HandleInputConnnection;
import dvl.srg.network.HandleOutputConnection;

import java.net.Socket;

/**
 * Created by administrator on 9/29/15.
 */
public class FactoryIOConnections {

    public static HandleOutputConnection newHandleOutputConnectionInstance(String host, int port) {
        return new HandleOutputConnection(host, port);
    }

    public static HandleOutputConnection newHandleOutputConnectionInstance(String host, int port, String message) {
        return new HandleOutputConnection(host, port, message);
    }

    public static HandleInputConnnection newHandleInputConnectionInstance(Socket connection) {
        return new HandleInputConnnection(connection);
    }
}