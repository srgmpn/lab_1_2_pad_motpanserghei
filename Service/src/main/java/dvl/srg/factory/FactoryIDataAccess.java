package dvl.srg.factory;

import dvl.srg.filesystem.JsonFSDataAccessImpl;
import dvl.srg.filesystem.XmlFSDataAccesImpl;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;
import dvl.srg.network.impl.ClientNetworkDataAccessImpl;
import dvl.srg.network.impl.ServerNetworkAccessImpl;

import java.net.Socket;
import java.nio.file.Path;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;

/**
 * Created by administrator on 9/28/15.
 */
public class FactoryIDataAccess {

    public static IDataAccess newIDataAccessInstance(int port, Path path) {

        if (port % 2 == 0) {
            return new JsonFSDataAccessImpl(path);
        }

        return new XmlFSDataAccesImpl(path);
    }

    public static IDataAccess newIDataAccessInstance(int port, Path path, Message message) {

        if (port % 2 == 0) {
            return new JsonFSDataAccessImpl(path, message);
        }

        return new XmlFSDataAccesImpl(path, message);
    }


    public static IDataAccess newIDataAccessInstance(Socket connection,
                                                     int secondProcessPort,
                                                     ArrayBlockingQueue<Message> messages,
                                                     boolean isServer,
                                                     boolean isSecondProcess,
                                                     ExecutorService service) {

        return new ServerNetworkAccessImpl(connection, secondProcessPort,  messages, isServer, isSecondProcess, service);
    }

    public static IDataAccess newIDataAccessInstance(String host,
                                                     int port,
                                                     String message,
                                                     ExecutorService service) {

        return new ServerNetworkAccessImpl(host, port, message, service);
    }

    public static IDataAccess newIDataAccessInstance(int port,
                                                     Socket connection,
                                                     ExecutorService service) {

        return new ClientNetworkDataAccessImpl( port, connection, service);
    }
}
