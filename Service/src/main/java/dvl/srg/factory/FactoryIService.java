package dvl.srg.factory;

import dvl.srg.interf.IService;
import dvl.srg.model.Message;
import dvl.srg.routing.RoutingService;
import dvl.srg.service.listener.impl.*;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by administrator on 9/28/15.
 */
public class FactoryIService {

    public static IService newServiceInstance(int port, AtomicBoolean isOk, String id) {

        if (id.equalsIgnoreCase("client")) {
            return new ServiceListenerClientImpl(port, isOk);
        }

        return new  ReceivePingService(port, isOk);
    }

    public static IService newServiceInstance(int port,
                                              ArrayBlockingQueue<Message> messages, boolean isServer,
                                              boolean isSecondProcess,
                                              int secondProcessPort,
                                              AtomicBoolean isOk) {
        return new ServiceListenerServerImpl(port, messages, isServer, isSecondProcess,  secondProcessPort, isOk);
    }



    public static IService newServiceInstance(ArrayBlockingQueue<Message> messages,
                                              Map<String, Integer> users,
                                              boolean isSecondServer, AtomicBoolean isOk,
                                              int secBrockerReceiverServicePort) {
        return new RoutingService(messages, users, isSecondServer, isOk, secBrockerReceiverServicePort);
    }

    public static IService newServiceInstance(String host, int port, AtomicBoolean isOk) {
        return new SendPingService(host, port, isOk);
    }

    public static IService newServiceInstance(int port,
                                              ArrayBlockingQueue<Message> messages,
                                              AtomicBoolean isOk) {
        return new ServiceListener2PClearQueueImpl(port, messages, isOk);
    }
}

