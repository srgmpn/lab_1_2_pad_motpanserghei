package dvl.srg.filesystem;

import dvl.srg.filesystem.abstr.AbstractFSDataAccess;
import dvl.srg.model.Message;
import dvl.srg.utils.XmlFormaterHelper;

import java.nio.file.Path;

/**
 * Created by administrator on 9/28/15.
 */
public class XmlFSDataAccesImpl extends AbstractFSDataAccess{

    public XmlFSDataAccesImpl(Path filePath, final Message message) {
        super(filePath, message);
    }

    public XmlFSDataAccesImpl(Path filePath) {
        super(filePath);
    }


    @Override
    public void readAsync() {
        fMessage = service.submit(() -> {
            Message messageFromCallble = null;
            messageFromCallble = XmlFormaterHelper.fromXmlFile(filePath.toFile());
            return messageFromCallble;
        });
    }

    @Override
    public void writeAsync() {
        //final Message finalMessage = message;
        service.submit(() -> {
        XmlFormaterHelper.toXmlFile(message, filePath.toFile());
        });
    }
}
