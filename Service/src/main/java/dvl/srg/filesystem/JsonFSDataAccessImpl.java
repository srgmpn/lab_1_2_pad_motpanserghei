package dvl.srg.filesystem;

import dvl.srg.filesystem.abstr.AbstractFSDataAccess;
import dvl.srg.model.Message;
import dvl.srg.utils.JsonFormaterHelper;

import java.io.*;
import java.nio.file.Path;

/**
 * Created by administrator on 9/28/15.
 */
public class JsonFSDataAccessImpl extends AbstractFSDataAccess {

    public JsonFSDataAccessImpl(Path filePath) {
        super(filePath);
    }
    public JsonFSDataAccessImpl(Path filePath, Message message) {
        super(filePath, message);
    }
/*
    public JsonFSDataAccessImpl(String filePath, String fileName, Message message) {
        super(filePath, fileName, message);
    }
*/
    @Override
    public void readAsync() {
        fMessage =  service.submit(() -> {
            Message messageFromCallble = null;
            try (BufferedReader br = new BufferedReader(new FileReader(filePath.toFile()))) {
               messageFromCallble = JsonFormaterHelper.fromJson(br);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return messageFromCallble;
        });
    }


    @Override
    public void writeAsync() {
        final String jsonMess = JsonFormaterHelper.toJson(message);
        //write in file in new Thread
        service.submit(() -> {
            try (FileWriter writer = new FileWriter(filePath.toFile());) {
                writer.write(jsonMess);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }}
