package dvl.srg.filesystem.abstr;

import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by administrator on 9/28/15.
 */
public abstract class AbstractFSDataAccess implements IDataAccess {

    protected Path filePath;
    protected final Message message;
    protected Future<Message> fMessage;
    protected ExecutorService service;

    public AbstractFSDataAccess(Path filePath) {
        this.filePath = filePath;
        message = null;
        service = Executors.newCachedThreadPool();
    }

    public AbstractFSDataAccess(Path filePath, final Message message) {
        this.filePath = filePath;
        this.message = message;
        service = Executors.newCachedThreadPool();
    }

    public Message getMessage() {
        try {
            return fMessage.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setFilePath(Path filePath) {
        this.filePath = filePath;
    }
}
