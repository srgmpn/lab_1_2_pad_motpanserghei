package dvl.srg.routing;

import dvl.srg.model.Message;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.utils.XmlFormaterHelper;


public class HandleRoutingMessage {
    private final Message message;

    private String stringMessage;
    private int userPortTo;
    private int userFormatDatesTo;

    public HandleRoutingMessage(Message message, int userPortTo){
        this.message = message;
        this.userPortTo = userPortTo;
    }

    //handle user message
    public String handle() {
        setUserFormatDates();
        formatStringMessage();
        return stringMessage;
    }

    //set user format dates 1 = xml, 0 = json
    private void setUserFormatDates() {
        userFormatDatesTo = userPortTo % 2;
    }

    //format data message from user
    private void formatStringMessage() {
        if (userFormatDatesTo == 0) {
            stringMessage = JsonFormaterHelper.toJson(message);
            return;
        }
        stringMessage = XmlFormaterHelper.toXml(message);
    }
}
