package dvl.srg.routing;

import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;
import dvl.srg.service.listener.abstr.AbstractService;
import dvl.srg.utils.Constants;
import dvl.srg.utils.TimerHelper;
import dvl.srg.winlog.WindowLog;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;


public class RoutingService extends AbstractService {

    private static final WindowLog winLogger = WindowLog.getWinLogger();

    private Map<String, Integer> users;
    private ArrayBlockingQueue<Message> messages;
    private Message message;
    private ExecutorService service;
    private String rcvMessage;
    private boolean isSecondServer;
    private int secBrockerReceiverServicePort;

    public RoutingService(ArrayBlockingQueue<Message> messages,
                          Map<String, Integer> users, boolean isSecondServer,
                          AtomicBoolean isOk, int secBrockerReceiverServicePort) {
        super(isOk);
        this.messages = messages;
        this.users = users;
        this.isSecondServer = isSecondServer;
        this.secBrockerReceiverServicePort = secBrockerReceiverServicePort;
        service = Executors.newFixedThreadPool(Constants.NR_THREAD_CONNECTIONS);
    }

    @Override
    public void service() {
        try {
            while (isOk.get()) {
                readMessageFromQueue();
                winLogger.showLog("Routing Service: " + message.toString());
                handleMessageRouting();
                sendMessageToReceiver();
                if (!isSecondServer) {
                    sendNotificationToSecondProcess();
                }
                TimerHelper.sleep(5);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //read a message form queue, if queue is empty thread will wait
    private void readMessageFromQueue() throws InterruptedException {
        System.out.println("Routing service wating message...");
        winLogger.showLog("\nRouting service wating message...");
        message = messages.take();
    }

    //handle received message
    private void handleMessageRouting() {
        System.out.println("Handle message routing");
        winLogger.showLog("\nHandle message routing");
        HandleRoutingMessage handleRoutingMessage =
                new HandleRoutingMessage(message, users.get(message.getUsernameTo()));
        rcvMessage = handleRoutingMessage.handle();
    }

    //send message to receiver
    private void sendMessageToReceiver() {
        System.out.println("Send data to receiver");
        winLogger.showLog("\nSend data to receiver msg: " + message.toString());
        IDataAccess writeData = FactoryIDataAccess.newIDataAccessInstance("127.0.0.1",
                users.get(message.getUsernameTo()), rcvMessage, service);
        writeData.writeAsync();
    }

    //send notification
    private void sendNotificationToSecondProcess() {
        System.out.println("Send notification to second process");
        winLogger.showLog("\nSend notification to second process...");
        IDataAccess writeData = FactoryIDataAccess.newIDataAccessInstance("127.0.0.1", secBrockerReceiverServicePort, "OK", service);
        writeData.writeAsync();
    }
}
