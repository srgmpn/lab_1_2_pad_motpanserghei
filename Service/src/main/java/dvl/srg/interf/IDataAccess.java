package dvl.srg.interf;


public interface IDataAccess {

    void readAsync();

    void writeAsync();
}
