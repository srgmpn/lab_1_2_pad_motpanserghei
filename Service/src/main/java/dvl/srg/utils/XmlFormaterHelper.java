package dvl.srg.utils;


import dvl.srg.model.Message;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlFormaterHelper {

    public static String toXml(Message message) {
        StringWriter xmlString = new StringWriter();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(dvl.srg.model.Message.class);
            JAXBElement<Message> je2 = new JAXBElement<>(new QName("message"), Message.class, message);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(je2, xmlString);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return xmlString.toString();
    }

    public static Message fromXml(StringReader xmlString) {
        Message message = null;
        StreamSource streamSource = new StreamSource(xmlString);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(dvl.srg.model.Message.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<Message> je1 = unmarshaller.unmarshal(streamSource, Message.class);
            message = je1.getValue();

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return message;
    }

    public static void toXmlFile(final Message message, File filePath) {
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(dvl.srg.model.Message.class);
            JAXBElement<Message> je2 = new JAXBElement<>(new QName("message"), Message.class, message);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(je2, filePath);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static Message fromXmlFile(File filePath) {
        Message message = null;
        try {
            StreamSource streamSource = new StreamSource(filePath);

            JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<Message> je1 = unmarshaller.unmarshal(streamSource, Message.class);
            message = /*(Message) unmarshaller.unmarshal(filePath);//*/je1.getValue();

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return message;
    }
}
