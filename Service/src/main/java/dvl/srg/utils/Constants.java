package dvl.srg.utils;


public class Constants {
    public static final int BACKLOG = 100;
    public static final int NR_THREAD_CONNECTIONS = 20;
}
