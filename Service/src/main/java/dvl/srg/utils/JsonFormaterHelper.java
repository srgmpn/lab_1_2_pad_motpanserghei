package dvl.srg.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dvl.srg.model.Message;

import java.io.BufferedReader;

public class JsonFormaterHelper {

    public static String toJson(Message message) {
        return new GsonBuilder().setPrettyPrinting().create().toJson(message);
    }

    public static Message fromJson(String jsonString) {
        return new Gson().fromJson(jsonString, Message.class);
    }

    public static Message fromJson(BufferedReader jsonString) {
        return new Gson().fromJson(jsonString, Message.class);
    }
}
