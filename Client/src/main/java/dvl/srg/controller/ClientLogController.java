package dvl.srg.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

public class ClientLogController {

    private Stage stage;
    private Scene scene;

    private int serverPort;
    private int listenerPort;
    private String host;
    private String userName;

    @FXML private Label userNameLabel;
    @FXML private ListView<String> sendListView;
    @FXML private ListView<String> recListView;
    @FXML private Button startButton;

    private ObservableList<String> sendItems;
    private ObservableList<String> recItems;


    public ClientLogController() {

    }

    public ClientLogController(ObservableList<String> sendItems, ObservableList<String> recItems) {
        this.sendItems = sendItems;
        this.recItems = recItems;
    }

    @FXML
    private void initialize() {

        sendItems = FXCollections.observableArrayList();
        recItems = FXCollections.observableArrayList();

        //userNameLabel.setText("Username: " + Main.getUserName());
        sendListView.setItems(sendItems);
        recListView.setItems(recItems);
    }

    @FXML
    public void start() {
        try{
            scene = new Scene((Parent) FXMLLoader.load(getClass().getResource("/clientlog.fxml")));

        } catch (Exception e) {
            e.printStackTrace();
        }

        stage = new Stage();
        stage.setTitle("Client Window Logs");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void startButtonHandler(ActionEvent event) {
/*
        new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ExecutorService service = Executors.newFixedThreadPool(32);

            IService serviceListener = new ServiceListenerClientImpl(20001, sendItems, recItems);
            service.submit(serviceListener);

            //Message message = new Message("srg", "john", "Mesaj Sergiu - Ion ");
            Message message = new Message("john", "srg", "Mesaj Sergiu - Ion ");
            sendItems.add(message.toString());
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            String messageXml = XmlFormaterHelper.toXml(message);
            IDataAccess sendData = new ServerNetworkAccessImpl(
                    "127.0.0.1",
                    9999,
                    messageXml,
                    service,
                    sendItems,
                    recItems);
            sendData.writeAsync();

            try {
                service.awaitTermination(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();*/
    }
}
