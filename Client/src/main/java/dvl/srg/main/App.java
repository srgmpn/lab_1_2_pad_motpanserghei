package dvl.srg.main;

import dvl.srg.controller.ClientLogController;
import dvl.srg.factory.FactoryDataType;
import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.factory.FactoryIService;
import dvl.srg.filesystem.abstr.AbstractFSDataAccess;
import dvl.srg.interf.IDataAccess;
import dvl.srg.interf.IService;
import dvl.srg.model.Message;
import dvl.srg.utils.TimerHelper;
import dvl.srg.winlog.WindowLog;
import javafx.application.Application;
import javafx.stage.Stage;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


public class App extends Application {


    public static void main(String[] args) {

        final WindowLog winLogger = WindowLog.getWinLogger();

        if (args.length == 0 || args.length > 2) {
            winLogger.showLog("Set corect  username and port!");
            return;
        }
        String userName = args[0];
        winLogger.setWindTitle("Client " + userName);

        int port = 0;
        try {
            port = Integer.parseInt(args[1]);

        } catch (NumberFormatException e) {
            winLogger.showLog("Set corect port value");
            return;
        }
        winLogger.showLog("UserName: + " + userName +"  PORT: " + port);

        // GENERATE USERS FILES AND COMPLETE WITH DEFAULT VALUES
        UserFileManagement userFileManagement = new UserFileManagement(userName, port);
        userFileManagement.populateUserFolderWithFile();

        //GET ALL FILES PATHS OF CURRENT USER
        ArrayList<Path> filesPath = userFileManagement.getFilesNames();

        //create a executor service
        ExecutorService service = Executors.newFixedThreadPool(32);

        IService serviceListener = FactoryIService.newServiceInstance(port, new AtomicBoolean(true), "client");
        service.submit(serviceListener);


        for (Path path: filesPath) {
            TimerHelper.sleep(10000);
            //get message
            Message message = getMessageFromFile(path, port);
            //translate to string
            String messageString = FactoryDataType.getUserFormatStringMessage(port, message);
            //send to receiver | 9999 default port which is listening to server
            IDataAccess sendData = FactoryIDataAccess.newIDataAccessInstance("127.0.0.1", 9999, messageString, service);
            sendData.writeAsync();
        }

        try {
            service.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

       //launch(args);
    }

    private static Message getMessageFromFile(Path path, int port) {
        IDataAccess readFromFile = FactoryIDataAccess.newIDataAccessInstance(port, path);

        readFromFile.readAsync();
        Message message = ((AbstractFSDataAccess)readFromFile).getMessage();

        return message;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        //userName = "srg";
        new ClientLogController().start();
    }
}
