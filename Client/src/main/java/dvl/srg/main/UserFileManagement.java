package dvl.srg.main;

import dvl.srg.factory.FactoryIDataAccess;
import dvl.srg.interf.IDataAccess;
import dvl.srg.model.Message;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 9/28/15.
 */
public class UserFileManagement {

    private static Map<String, Integer> usersDates = new HashMap<>();
    private static ArrayList<Path> filesPath = new ArrayList<>();

    private String userName;
    private int port;


    public UserFileManagement(String userName, int port) {
        this.userName = userName;
        this.port = port;
        fillUserDatesMap();
    }

    public void populateUserFolderWithFile() {

        int i = 1;
        for (Map.Entry<String, Integer> entry : usersDates.entrySet()) {
            String key = entry.getKey();

            if (!key.equalsIgnoreCase(userName)) {
                Message message = new Message(userName,
                        key,
                        "Message from " + userName + " to " + key + " nr " + i + ".");
                saveToFile(message);
                ++i;
            }
        }
    }

    private void saveToFile(Message message) {
        Path path = null;
        if (port % 2 == 0) {
            path = getPath(userName, "out", userName + System.currentTimeMillis(), "json");
        } else {
            path = getPath(userName, "out", userName + System.currentTimeMillis(), "xml");
        }
        IDataAccess writer = FactoryIDataAccess.newIDataAccessInstance(port, path, message);
        writer.writeAsync();
    }

    private static void fillUserDatesMap() {
        usersDates.put("srg", 20000);
        usersDates.put("john", 20001);
        usersDates.put("grid", 20002);
    }

    private Path getPath(String userName, String type, String name, String fileType) {
        Path newFilePath = null;
        try {
            Files.createDirectories(Paths.get(Paths.get("").toAbsolutePath().toString() + "/files/" + userName +"/" + type));
            newFilePath = Paths.get(Paths.get("").toAbsolutePath().toString() + "/files/" + userName +"/" + type +"/", name + "." + fileType);
            Files.createFile(newFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        filesPath.add(newFilePath);
        return newFilePath;
    }

    public ArrayList<Path> getFilesNames() {
        return filesPath;
    }
}
